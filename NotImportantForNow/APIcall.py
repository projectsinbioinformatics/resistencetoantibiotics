from Bio.Blast import NCBIWWW
from Bio import SeqIO
from Bio.Blast import NCBIXML
import sys,os
# python APIcall.py Data/ResistenceGenes/EColi-K12-Penicilin/EColi-K12-penicilin-cloxacilin.fasta ref_prok_rep_genomes mT
# Escherichia, wgs, nt,

# makeblastdb -in Data/EColiGenomes/Genomes/Escherichia_albertii_TW07627-WHS -out myDatabase -parse_seqids -dbtype nucl
# magicblast -query Data/ResistenceGenes/EColi-K12-Penicilin/EColi-K12-penicilin-cloxacilin.fasta -db myDatabase

# Database to search:
# prok_complete_genomes <- use this one
# ref_prok_rep_genomes
# nt or nr

DEBUG = False
args = sys.argv

outputFolder = "OUTPUT"
speciesSearchingFor = ["Shigella".upper(),"Escherichia".upper()]


if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)


def alignWithBlast(record, database, filter, strainAligingWith):

    result_handle = NCBIWWW.qblast(database=database, sequence=record.format("fasta"),
                                   program="blastn", filter=filter)

    blast_record = NCBIXML.read(result_handle)

    for alignment in blast_record.alignments:
        if DEBUG:
            print(alignment.__dict__.keys())
            print("Reference Genome title: ", alignment.title)
            print("Reference Genome definition: ", alignment.hit_def)
            print("Length of Ref genome: ", alignment.length)

        if "Escherichia".upper() in alignment.hit_def.upper() or "Shigella".upper() in alignment.hit_def.upper():
            if strainAligingWith.upper() in alignment.hit_def.upper() or strainAligingWith.upper() in alignment.title:
                if not os.path.exists(handleFileName(alignment.hit_def,alignment.title)):
                    os.makedirs(handleFileName(alignment.hit_def,alignment.title))
                for hsp in alignment.hsps:
                    if DEBUG:
                        print("---------------------------------------------")
                        print("Dictionary keys", hsp.__dict__.keys())
                        print("Index start of ref seq", hsp.sbjct_start) # index from of ref seq
                        print("Index end of ref seq", hsp.sbjct_end) #index to of ref seq
                        print(hsp.sbjct)
                        print(hsp.match)
                        print(hsp.query)
                        print("\n\n")
                    if hsp.expect < 0.05:
                        writeToFile(hsp.sbjct_end, hsp.sbjct_start, hsp.sbjct, hsp.query, alignment.hit_def, alignment.title, str(hsp.expect).replace(".","-"))

def handleFileName(alignHitDef, alignTitle):
    fileName = alignHitDef.replace(" ", "").replace(".","").replace("complete", "").replace(",","") +"--"+ alignTitle.replace(" ", "").replace(".","").replace("complete", "").replace(",","").replace("/","")
    if len(fileName) > 150:
        fileName = fileName[0:150]

    filePath = outputFolder + "/"+fileName
    return filePath

def validateNameSearchingFor(hit_def, title):
    if (hit_def in speciesSearchingFor) :
        return True


def writeToFile(end,start, subject, query, hitdef , title,length):
    fileName = ""
    if hitdef == None:
        fileName = "something"
    else:
        fileName = str(end) + "--" + str(start) + "-"+str(length[0:4])

    with open(handleFileName(hitdef, title) + "/" + fileName + ".csv", 'w') as fh:
        fh.writelines("position" + ",subject" +",query"+ "\n")
        subject = list(subject)
        query = list(query)
        row = 0
        if start < end:
            for i in range(start, end):
                fh.writelines(str(i) + "," + str(subject[row]) + "," + str(query[row]) + "\n")
                row = row + 1
        else:
            for i in range(end, start):
                fh.writelines(str(i) + "," + str(subject[row]) + "," + str(query[row]) + "\n")
                row = row + 1


    fh.close()


record = SeqIO.read(args[1], "fasta")
seq = record.seq
mylist = alignWithBlast(record, args[2], args[3], args[4])

