from Bio import SeqIO
import sys,os

from Bio.Alphabet import IUPAC
from Bio.Alphabet import generic_dna, generic_protein,generic_nucleotide
from Bio.Seq import Seq
from Bio import Entrez


args = sys.argv
finalOutput = ""
cnt = 0

def readFile(file):
    records = SeqIO.parse(file, "fasta")
    for record in records:
        isSpeciesFound("Escherichia", "penicillin", record)


def isSpeciesFound(speciesName, antibioticName, record):
    global cnt
    if speciesName in record.description[record.description.find('['):record.description.find(']')] or "Shigella" in record.description[record.description.find('['):record.description.find(']')]:
        #if antibioticName in record.description:
        cnt = cnt + 1
        print(cnt)
        addToOutput(record)


def addToOutput(record):
    global finalOutput

    seq = "\n>" + str(record.id) + str(record.description) + "\n" + str(record.seq)
    #finalOutput = finalOutput + seq
    writeToFile("ourgenes.fasta", seq)

def retroTranslation(record):
    try:
        prot_id = record.id
        Entrez.email = "alekss.ro@gmail.com"
        handle = Entrez.efetch(id=prot_id, db="protein", rettype="gb", retmode="gb",program="tblastn")
        ret = SeqIO.read(handle, 'genbank')
        handle.close()

        if len(ret._seq) < 1000000:
            nt_id = ret.annotations["db_source"]

            Entrez.email = "alekss.ro@gmail.com"
            handle = Entrez.efetch(id=nt_id, db="nucleotide", rettype="gb", retmode="fasta")
            ret = SeqIO.read(handle, 'genbank')
            handle.close()
            return ret
    except (RuntimeError, TypeError, NameError,ValueError):
        print("")


def writeToFile(fileName, finalOutput):
    #print(cnt)
    with open(fileName, 'a+') as f:
        f.write(str(finalOutput))
        f.close()

    return False

readFile("card-data.fasta.fasta/nucleotide_fasta_protein_homolog_model.fasta")
