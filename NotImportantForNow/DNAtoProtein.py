from Bio import SeqIO
from Bio.Seq import Seq

gencode = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W'}


def codon_dict( filler=0 ):
    """Return a dictionary of codon:filler pairs for all 64 codons."""
    d = {}
    for one in 'AGCT':
        for two in 'AGCT':
            for three in 'AGCT':
                d.update({ one+two+three:filler })
    return d

def codon_frequencies( seq ):
    """Return a dictionary of codon:frequency pairs for all 64 codons."""
    table = codon_dict()
    for i in range(len(seq)):
        try: table[seq[i:i+3]] += 1
        except: pass
    seq = seq[::-1]
    for i in range(len(seq)):
        try: table[seq[i:i+3]] += 1
        except: pass
    return table

genome = SeqIO.read("Data/EColiGenomes/Genomes/EscherichiaColi-E24377A.fasta", "fasta")

print(codon_frequencies(genome.seq))
