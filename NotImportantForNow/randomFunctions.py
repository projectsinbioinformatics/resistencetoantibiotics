def retrieveNCBIrecord(id, db="nucleotide", rettype="gb"):
    """INPUT: id=NCBI Accesion Number. OUTPUT: """
    Entrez.email = "alekss.ro@outlook.es"
    handle = Entrez.efetch(id=id, db=db, rettype=rettype)
    record = SeqIO.read(handle, format = rettype)
    handle.close

    return record

def readFile(file):
    records = SeqIO.parse(file, "fasta")
    for record in records:
        isSpeciesFound("Escherichia", "penicillin", record)


def isSpeciesFound(speciesName, antibioticName, record):
    global cnt
    if speciesName in record.description[record.description.find('['):record.description.find(']')]:
        #if antibioticName in record.description:
        my_protein = Seq("".join(record.seq), generic_nucleotide)
        cnt = cnt + 1
        record = retroTranslation(record)
        print(record)
        addToOutput(record)

def addToOutput(record):
    global finalOutput
    seq = "\n>" + record.id + record.description + "\n" + record.seq
    finalOutput = finalOutput + seq

def retroTranslation(record):

    prot_id = record.id
    Entrez.email = "alekss.ro@gmail.com"
    handle = Entrez.efetch(id=prot_id, db="protein", rettype="gb", retmode="gb")
    ret = SeqIO.read(handle,'genbank')
    handle.close

    nt_id = ret.annotations["db_source"]

    Entrez.email = "alekss.ro@gmail.com"
    handle = Entrez.efetch(id=nt_id, db="nucleotide", rettype="gb", retmode="fasta")
    ret=SeqIO.read(handle,'genbank')
    handle.close
    return ret


def writeToFile(fileName, finalOutput):
    print(cnt)
    with open(fileName, 'a+') as f:
        f.write(str(finalOutput))
        f.close()

    return



###############################################################################

# GC content
# Plasmid or genome
# Specie
# Length of gene or genome or ratio
# Find Ori by GC skew and -> distance to Ori
# Strand (is annotated in the fasta files)


# inputs = SeqIO.read(args[1], "fasta")
# for inputs in SeqIO.parse("example.fasta", "fasta"):
#     print(record.id)

# for inputs in SeqIO.parse(args[1], "fasta"):
#     sol = retrieveNCBIrecord(id = inputs.id)
#     SeqIO.write(sol, args[2], "fasta")


# sol = retrieveNCBIrecord("NC_002516.2", rettype="fasta")
# print(sol)
#
# sol = retrieveNCBIrecord("ACT97415.1", db="protein")
# print(sol)
