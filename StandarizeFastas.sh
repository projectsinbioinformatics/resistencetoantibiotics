#!/bin/bash

for filename in Data-Fasta/Raw/NormalGenes/*; do
    python3 change_standard_of_fasta.py "$filename" "Data-Fasta/Processed/standardized$(basename "$filename" .fasta).fasta"
done
