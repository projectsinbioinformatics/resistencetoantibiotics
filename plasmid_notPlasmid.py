from Bio import SeqIO
import sys
from Bio import Entrez

args = sys.argv


def retrieveNCBIrecord(id, db="nucleotide", rettype="gb"):
    """INPUT: id=NCBI Accesion Number. OUTPUT: """
    Entrez.email = "alekss.ro@outlook.es"
    handle = Entrez.efetch(id=id, db=db, rettype=rettype)
    record = SeqIO.read(handle, format=rettype)
    handle.close

    return record


def isPlasmid(record):
    try:
        parts = record.id.split("|")
        record = retrieveNCBIrecord(parts[1])
        for feature in record.features:
            mol_type = feature.qualifiers.get("plasmid", "")
            # print(mol_type)
            if mol_type:
                return str(mol_type[0])
            else:
                return "Genomic"
    except (ConnectionError, ConnectionAbortedError, ConnectionRefusedError, RuntimeError):
        print("Something went wrong. Brace yourself")
        pass
