import sys, os, rpy2
import rpy2.robjects as robjects
import rpy2.robjects.packages as rpackages
from rpy2.robjects.packages import importr
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage
import urllib

r = robjects.r
r.source("BuildAModelAndPredict.R")
