from Bio.Blast import NCBIWWW
from Bio import SeqIO
from Bio.Blast import NCBIXML
import sys,os
from Bio import GenBank
from Bio import Entrez


args = sys.argv
finalOutput = ""
cnt = 0
speciesNames = ["Escherichia", "Shigella", "Plasmid", "plasmid"]

def addToOutput(record):
    global finalOutput
    seq = "\n>" + record.description + "\n" + record.seq
    finalOutput = finalOutput + seq

def writeToFile(fileName, finalOutput):
    print(cnt)
    with open(fileName, 'a+') as f:
        f.write(str(finalOutput))
        f.close()
    return

fileIN = str(args[1])
records = SeqIO.parse(fileIN, "fasta")
for record in records:
    for specie in speciesNames:
        if specie in record.description[record.description.find('['):record.description.find(']')]:
            print(record)
            addToOutput(record)

writeToFile(args[2], finalOutput)
