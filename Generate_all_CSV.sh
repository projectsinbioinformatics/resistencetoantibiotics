#!/bin/bash

for filename in Data-Fasta/Processed/*; do
    python3 make_csv_from_fasta.py "$filename" "../Data-Csv/$(basename "$filename" .fasta).csv"
done
